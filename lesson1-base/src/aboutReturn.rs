fn p1() -> f64 {
    3.1415926
}

fn not_p1() {
    3.1415926;
}

fn main() {
    let is_pi = p1();
    let is_unit1 = not_p1();
    let is_unit2 = {
        p1();
    };

    println!("is_pi: {:?}, is_unit1: {:?}, is_unit2: {:?} ", is_pi, is_unit1, is_unit2); // is_pi: 3.1415926, is_unit1: (), is_unit2: ()
}